from unittest import TestCase
from flask import Flask, url_for, redirect
from flask_urlsigning import URLSigning, signed_url_for, verify_signed_url

class TestRequiresSecretKeySet(TestCase):
    def setUp(self):
        self.app = Flask(__name__)

    def test_with_secret_key_not_set(self):
        with self.assertRaises(RuntimeError):
            URLSigning().init_app(self.app)

    def test_with_secret_key_set(self):
        self.app.secret_key = 'test-key'
        URLSigning().init_app(self.app)

class TestUrlSigning(TestCase):
    def setUp(self):
        app = Flask(__name__)
        app.debug = True
        app.secret_key = 'test-key'

        @app.route('/target')
        def target():
            if verify_signed_url():
                return '', 200
            return '', 403

        @app.route('/signed-redirect')
        def signed_redirect():
            return redirect(signed_url_for('target'))

        @app.route('/unsigned-redirect')
        def unsigned_redirect():
            return redirect(url_for('target'))

        self.app = app

    def test_unsigned(self):
        with self.app.test_client() as client:
            resp = client.get('/unsigned-redirect', follow_redirects=True)
            self.assertEqual(resp.status_code, 403)

    def test_signed(self):
        with self.app.test_client() as client:
            resp = client.get('/signed-redirect', follow_redirects=True)
            self.assertEqual(resp.status_code, 200)
